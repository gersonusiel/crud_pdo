<?php
include "../../class/Carrera.class.php";
$carrera = new Carrera();

if (isset($_POST['submit'])) {
    $nombreCarrera = $_POST['carrera'];
    $titulo = $_POST['titulo'];
    $facultad = $_POST['facultad'];

    if (trim($nombreCarrera)=='' || trim($titulo)=='' || trim($facultad)=='') {
        header("Location: frmAgregar.php?r=error");
    }
    else{
    $carrera->nombre = utf8_decode($nombreCarrera);
    $carrera->titulo = utf8_decode($titulo);
    $carrera->codFacultad = $facultad;
    $carrera->save();
    echo "<script>alert('Registro guardado correctamente'); opener.location.reload(); window.close();</Script>";
    }
}

?>
