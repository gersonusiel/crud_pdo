<?php
include "../../class/Carrera.class.php";
$carrera = new Carrera();

if (isset($_POST['submit'])) {
    $codigo = $_POST['codigo'];
    $nombreCarrera = $_POST['carrera'];
    $titulo = $_POST['titulo'];
    $facultad = $_POST['facultad'];

    if (trim($nombreCarrera)=='' || trim($titulo)=='' || trim($facultad)=='') {
        header("Location: frmEditar.php?r=error&id=".$codigo."");
    }
    else{
    $carrera->codigo = $codigo;
    $carrera->nombre = $nombreCarrera;
    $carrera->titulo = $titulo;
    $carrera->codFacultad = $facultad;
    $carrera->update();
    echo "<script>alert('Registro editado correctamente'); opener.location.reload(); window.close();</Script>";
    }
}

?>
