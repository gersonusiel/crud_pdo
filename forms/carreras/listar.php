<?php
include "../../class/Carrera.class.php";
include "../../class/Facultad.class.php";
$carrera = new Carrera();
$facultad = new Facultad();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carreras</title>
    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- FONT AWESOME -->
    <script src="https://kit.fontawesome.com/87b8bff04b.js" crossorigin="anonymous"></script>
    <!-- STYLES -->
    <link rel="stylesheet" href="../../css/listar.css">
</head>

<body>

    <div class="opciones text-center p-4">
        <a onclick="javascript:formNuevo();" class="bg-primary">Nueva carrera</a>
        <a href="../../index.php" class="bg-danger">Cerrar</a>
    </div>
    <table class="table table-striped table-bordered container  mt-3 bg-light">
        <thead class="bg-info text-white">
            <tr>
                <th scope="col">Carrera</th>
                <th scope="col">Título</th>
                <th scope="col">Facultad</th>
                <th scope="col" colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php
                $data = $carrera->getAll();
                foreach ($data as  $key => $value) {
                    //Obtener la facultad
                    $facultad->codigo = $value['codFacultad'];
                    $arrFacultad = $facultad->getByCodFacultad();
                    $id = $value['codCarrera'];
                    echo "<td>";
                    echo utf8_encode($value['nombreCarrera']);
                    echo "</td><td>";
                    echo utf8_encode($value['titulo']);
                    echo "</td><td>";
                    foreach ($arrFacultad as $key => $row) {
                        echo utf8_encode($row['nombreFacultad']);
                    }
                    echo "</td>";
                    echo "<td class='icon'><a onclick='javascript:formEditar(" . $id . ");' title='Editar'><i class='fa fa-edit'></i></a></td>";
                    echo "<td class='icon'><a onclick='javascript:borrar(" . $id . ");' title='Eliminar'><i class='fa fa-trash-alt red'></i></a></td></tr>";
                }
                ?>
        </tbody>
    </table>
    <script type="text/javascript">
        function borrar(id) {
            var resp = confirm("Seguro que desea eliminar el registro?");
            if (resp == true) {
                window.location.href = "eliminar.php?id=" + id + "";
            }
        }

        function formNuevo() {
            window.open('frmAgregar.php', 'frmAgregar', 'width=600,height=600');
        }

        function formEditar(id) {
            var resp = confirm("Seguro que desea editar el registro?");
            if (resp == true) {
                window.open('frmEditar.php?id=' + id + "", 'frmEditar', 'width=600,height=600');
            }
        }
    </script>

</body>

</html>