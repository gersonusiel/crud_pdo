<?php
include "../../class/Carrera.class.php";
$carrera = new Carrera();

include "../../class/Facultad.class.php";
$facultad = new Facultad();
$arrFacultad = $facultad->getAll();


if(isset($_GET['r'])){
    if($_GET['r']== 'error'){
        echo "<div class=\"alert alert-danger text-center alert-dismissible fade show m-3\" role=\"alert\">
            <strong>Todos los campos son obligatorios!</strong> 
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
            </button>
        </div>";
    }
}

if (isset($_GET['id'])) {
    $carrera->codigo = $_GET['id'];
    $arrCarrera = $carrera->getByCod();
    $facultadActual = $arrCarrera[0]['codFacultad'];
?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Editar Carrera</title>
        <!-- BOOTSTRAP 4.4.1 -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container">
            <h1 class="text-center text-primary">Editar carrera</h1>
            <form class="col-md-8" action="editar.php" method="POST">
                <input type="hidden" name="codigo" value="<?= utf8_encode($arrCarrera[0]['codCarrera']) ?>">
                <div class="form-group">
                    <label for="carrera">Carrera</label>
                    <input type="text" class="form-control" id="carrera" name="carrera" value="<?= utf8_encode($arrCarrera[0]['nombreCarrera']) ?>">
                </div>
                <div class="form-group">
                    <label for="titulo">Título</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" value="<?= utf8_encode($arrCarrera[0]['titulo']) ?>">
                </div>
                <div class="form-group">
                    <label for="facultad">Facultad</label>
                    <select id="facultad" class="form-control" name="facultad">
                        <?php
                        foreach ($arrFacultad as $key => $value) {
                            if ($value['codFacultad'] == $facultadActual) {
                                echo "<option selected value=" . $value['codFacultad'] . ">" . utf8_encode($value['nombreFacultad']) . "</option>";
                            } else {
                                echo "<option value=" . $value['codFacultad'] . ">" . utf8_encode($value['nombreFacultad']) . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" name="submit" value="Guardar"></input>
                <button type="reset" onclick="javascript:cerrar();" class="btn btn-secondary">Cancelar</button>
            </form>
        </div>
    <?php } ?>
    <script type="text/javascript">
        function cerrar(){
            window.close();
        }
    </script>
    </body>

    </html>