<?php
include "../../class/Carrera.class.php";
$carrera = new Carrera();
$arrCarrera = $carrera->getAll();

include "../../class/Alumno.class.php";
$alumno = new Alumno();

include "direccion.php";

if (isset($_GET['r'])) {
    if ($_GET['r'] == 'error') {
        echo "<div class=\"alert alert-danger text-center alert-dismissible fade show m-3\" role=\"alert\">
            <strong>Todos los campos son obligatorios!</strong> 
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
            </button>
        </div>";
    }
}

if (isset($_GET['id'])) {
    $alumno->codigo = $_GET['id'];
    $arrAlumno = $alumno->getByCod();
    $carreraActual = $arrAlumno[0]['codCarrera'];
?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Editar Estudiante</title>
        <!-- BOOTSTRAP 4.4.1 -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container">
            <h1 class="text-center text-primary">Editar estudiante</h1>
            <form class="col-md-8" action="editar.php" method="POST">
                <input type="hidden" name="codigo" value="<?= utf8_encode($arrAlumno[0]['codAlumno']) ?>">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" require value="<?= utf8_encode($arrAlumno[0]['nombreCompleto']) ?>">
                </div>
                <div class="form-group">
                    <label for="tipoDocumento">Tipo de documento</label>
                    <select id="tipoDocumento" class="form-control" name="tipoDocumento" onchange="respuesta();">
                        <?php if ($arrAlumno[0]['tipoDocumento'] == 1) { ?>
                            <option value="1" selected>DUI</option>
                            <option value="2">Pasaporte</option>
                        <?php } else { ?>
                            <option value="1">DUI</option>
                            <option value="2" selected>Pasaporte</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="numDocumento">Número de documento</label>
                    <input type="text" class="form-control" id="numDocumento" name="numDocumento" placeholder="000000000-0" value="<?= utf8_encode($arrAlumno[0]['numeroDocumento']) ?>">
                </div>
                <div class="form-group">
                    <label for="genero" class="d-block">Género</label>
                        <div class="form-check form-check-inline">
                        <label class="form-check-label mr-1" for="rdbMasculino">Masculino</label>
                        <input class="form-check-input" type="radio" name="genero" id="rdbMasculino" value="Masculino" 
                        <?php if ($arrAlumno[0]['sexo'] == 'Masculino') {echo "checked";}?>>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label mr-1" for="rdbFemenino">Femenino</label>
                        <input class="form-check-input" type="radio" name="genero" id="rdbFemenino" value="Femenino"
                        <?php if ($arrAlumno[0]['sexo'] == 'Femenino') {echo "checked";}?>>
                    </div>
                </div>
                <div class="form-group">
                    <label for="correo">Correo</label>
                    <input type="email" class="form-control" id="correo" name="correo" value="<?= utf8_encode($arrAlumno[0]['correo']) ?>">
                </div>
                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?= utf8_encode($arrAlumno[0]['numeroTelefono']) ?>">
                </div>
                <div class="form-group">
                    <label for="direccion">Dirección</label>
                    <select id="direccion" class="form-control" name="direccion">
                        <?php
                        foreach ($direcciones as $direccion) {
                            if (utf8_encode($arrAlumno[0]['direccion']) == utf8_decode(utf8_encode($direccion))) {
                                echo "<option selected>" . utf8_decode(utf8_encode($direccion)) . "</option>";
                            } else {
                                echo "<option>" . utf8_decode(utf8_encode($direccion)) . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="carrera">Carrera</label>
                    <select id="carrera" class="form-control" name="codCarrera">
                        <?php
                        foreach ($arrCarrera as $key => $value) {
                            if ($value['codCarrera'] == $carreraActual) {
                                echo "<option selected value=" . $value['codCarrera'] . ">" . utf8_encode($value['nombreCarrera']) . "</option>";
                            } else {
                                echo "<option value=" . $value['codCarrera'] . ">" . utf8_encode($value['nombreCarrera']) . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" name="submit" value="Guardar"></input>
                <button type="reset" onclick="javascript:cerrar();" class="btn btn-secondary">Cancelar</button>
            </form>
        </div>
    <?php } ?>
    <script type="text/javascript">
        function cerrar() {
            window.close();
        }

        function cerrar() {
            window.close();
        }

        function respuesta() {
            var txt = document.getElementById('numDocumento');
            var val = document.getElementById('tipoDocumento');

            if (val.value == "1") {
                // alert("DUI");			
                var placeholder = "000000000-0";
                txt.value = '';
                txt.placeholder = placeholder;
            } else if (val.value == "2") {
                //alert("pasaporte");
                var placeholder = "A0000000";
                txt.value = '';
                txt.placeholder = placeholder;
            }

        }
    </script>
    </body>

    </html>