<?php
include "../../class/Carrera.class.php";
include "../../class/Alumno.class.php";
$alumno = new Alumno();
$carrera = new Carrera();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Estudiantes</title>
    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- FONT AWESOME -->
    <script src="https://kit.fontawesome.com/87b8bff04b.js" crossorigin="anonymous"></script>
    <!-- STYLES -->
    <link rel="stylesheet" href="../../css/listar.css">
</head>

<body>

    <div class="opciones text-center p-4">
        <a onclick="javascript:formNuevo();" class="bg-primary">Nuevo estudiante</a>
        <a href="../../index.php" class="bg-danger">Cerrar</a>
    </div>
    <div class="row d-flex justify-content-center col-md-12">
        <table class="table table-striped table-bordered col-md-11 table-sm bg-light">
            <thead class="bg-info text-white">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Género</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Carrera</th>
                    <th scope="col" colspan="2"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php
                    $data = $alumno->getAll();
                    foreach ($data as  $key => $value) {
                        //Obtener la facultad
                        $carrera->codigo = $value['codCarrera'];
                        $arrCarrera = $carrera->getByCod();
                        $id = $value['codAlumno'];
                        echo "<td>";
                        echo utf8_encode($value['nombreCompleto']);
                        echo "</td><td>";
                        echo $value['numeroDocumento'];
                        echo "</td><td>";
                        echo $value['sexo'];
                        echo "</td><td>";
                        echo utf8_encode($value['correo']);
                        echo "</td><td>";
                        echo $value['numeroTelefono'];
                        echo "</td><td>";
                        echo utf8_encode($value['direccion']);
                        echo "</td><td>";
                        foreach ($arrCarrera as $key => $row) {
                            echo utf8_encode($row['nombreCarrera']);
                        }
                        echo "</td>";
                        echo "<td class='icon'><a onclick='javascript:formEditar(" . $id . ");' title='Editar'><i class='fa fa-edit'></i></a></td>";
                        echo "<td class='icon'><a onclick='javascript:borrar(" . $id . ");' title='Eliminar'><i class='fa fa-trash-alt red'></i></a></td></tr>";
                    }
                    ?>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        function borrar(id) {
            var resp = confirm("Seguro que desea eliminar el registro?");
            if (resp == true) {
                window.location.href = "eliminar.php?id=" + id + "";
            }
        }

        function formNuevo() {
            window.open('frmAgregar.php', 'frmAgregar', 'width=600,height=600');
        }

        function formEditar(id) {
            var resp = confirm("Seguro que desea editar el registro?");
            if (resp == true) {
                window.open('frmEditar.php?id=' + id + "", 'frmEditar', 'width=600,height=600');
            }
        }
    </script>

</body>

</html>