<?php
include "../../class/Carrera.class.php";
include "../../class/Alumno.class.php";
$alumno = new Alumno();

if (isset($_POST['submit'])) {
    $nombre = $_POST['nombre'];
    $tipoDocumento = $_POST['tipoDocumento'];
    $numDocumento = $_POST['numDocumento'];
    $genero = $_POST['genero'];
    $correo = $_POST['correo'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['direccion'];
    $codCarrera = $_POST['codCarrera'];

    if (trim($nombre)=='' || trim($tipoDocumento)=='' || trim($numDocumento)=='' || trim($genero)=='' || trim($telefono)=='' || trim($direccion)=='' || trim($codCarrera)=='' ){
        header("Location: frmAgregar.php?r=error");
    }
    else{
    $alumno->nombre = utf8_decode($nombre);
    $alumno->tipoDocumento = $tipoDocumento;
    $alumno->numeroDocumento = $numDocumento;
    $alumno->sexo = $genero;
    $alumno->correo = utf8_decode($correo);
    $alumno->telefono = $telefono;
    $alumno->direccion = utf8_decode($direccion);
    $alumno->codCarrera = $codCarrera;
    $alumno->save();
    echo "<script>alert('Registro guardado correctamente'); opener.location.reload(); window.close();</Script>";
    }
}

?>
