<?php
include "../../class/Carrera.class.php";
include "../../class/Alumno.class.php";
$alumno = new Alumno;

if(isset($_GET['id'])){
    $alumno->codigo = $_GET['id'];
    $alumno->delete();
    echo "<script>alert('Registro eliminado correctamente');</script>";
    header( "Refresh:0; url=listar.php");
}
