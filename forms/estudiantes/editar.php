<?php
include "../../class/Carrera.class.php";
include "../../class/Alumno.class.php";
$alumno = new Alumno();

if (isset($_POST['submit'])) {
    $codigo = $_POST['codigo'];
    $nombre = $_POST['nombre'];
    $tipoDocumento = $_POST['tipoDocumento'];
    $numDocumento = $_POST['numDocumento'];
    $genero = $_POST['genero'];
    $correo = $_POST['correo'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['direccion'];
    $codCarrera = $_POST['codCarrera'];

    if (trim($nombre)=='' || trim($tipoDocumento)=='' || trim($numDocumento)=='' || trim($genero)=='' || trim($correo)=='' || trim($telefono)=='' || trim($direccion)=='' || trim($codCarrera)=='' ){
        header("Location: frmEditar.php?r=error&id=".$codigo."");
    }
    else{
    $alumno->codigo = $codigo;
    $alumno->nombre = utf8_decode($nombre);
    $alumno->tipoDocumento = $tipoDocumento;
    $alumno->numeroDocumento = $numDocumento;
    $alumno->sexo = $genero;
    $alumno->correo = utf8_decode($correo);
    $alumno->telefono = $telefono;
    $alumno->direccion = utf8_decode($direccion);
    $alumno->codCarrera = $codCarrera;
    $alumno->update();
    echo "<script>alert('Registro editado correctamente'); opener.location.reload(); window.close();</Script>";
    }
}

?>
