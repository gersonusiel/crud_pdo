<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD PDO</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <div class="content-menu">
        <div class="menu">
            <a href="forms/carreras/listar.php" class="btn">
                Carreras
            </a>
            <a href="forms/estudiantes/listar.php" class="btn">
                Estudiates
            </a>
        </div>
    </div>
    <div class="integrantes">
        <ul>
            <li><strong>Marcos Daniel Rubí Hernández</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SMIS925620</li>
            <li><strong>Gerson Usiel Quintanilla Sánchez</strong> &nbsp;&nbsp;SMIS004718</li>
        </ul>
    </div>
</body>

</html>