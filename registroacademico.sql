/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100140
 Source Host           : localhost:3306
 Source Schema         : registroacademico

 Target Server Type    : MySQL
 Target Server Version : 100140
 File Encoding         : 65001

 Date: 18/04/2020 15:23:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alumno
-- ----------------------------
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE `alumno`  (
  `codAlumno` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCompleto` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `tipoDocumento` int(11) NULL DEFAULT NULL,
  `numeroDocumento` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `sexo` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `correo` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `numeroTelefono` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `direccion` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `codCarrera` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codAlumno`) USING BTREE,
  INDEX `FK_carrera`(`codCarrera`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of alumno
-- ----------------------------
INSERT INTO `alumno` VALUES (1, 'Juan Alberto Flores Morales', 1, '043589939-2', 'Masculino', 'juan@gmail.com', '7582-6943', 'Usulután', 1);
INSERT INTO `alumno` VALUES (2, 'Alejandra Sofia Silva Canizalez', 1, '095539939-2', 'Femenino', 'asilva@gmail.com', '7292-5789', 'San Miguel', 1);
INSERT INTO `alumno` VALUES (3, 'Maria de los Angeles Gonzalez Flores', 1, '092223579-2', 'Femenino', 'mariaG@gmail.com', '7500-7543', 'San Miguel', 2);
INSERT INTO `alumno` VALUES (4, 'Jose Alberto Castillo Flores', 1, '087899639-2', 'Masculino', 'albcastillo@gmail.com', '7550-5689', 'San Miguel', 2);
INSERT INTO `alumno` VALUES (5, 'Dina Argueta Molina', 1, '022553669-2', 'Femenino', 'dina2142@gmail.com', '7382-5963', 'San Miguel', 3);
INSERT INTO `alumno` VALUES (6, 'Wilfredo Antonio Ganuza Portillo', 1, '075896347-3', 'Masculino', 'wil.ganuza@gmail.com', '7222-8999', 'San Miguel', 3);
INSERT INTO `alumno` VALUES (7, 'Carmen Lourdes Linares Lazo', 1, '089735439-6', 'Femenino', 'clourdes@gmail.com', '2645-8963', 'San Miguel', 4);
INSERT INTO `alumno` VALUES (8, 'Luis Jose Ventura Ventura', 1, '087412369-8', 'Masculino', 'jventura@gmail.com', '2296-3545', 'San Miguel', 5);
INSERT INTO `alumno` VALUES (9, 'Claudia Estela Villalobos Ventura', 1, '089754398-4', 'Femenino', 'cl214@gmail.com', '7789-4152', 'Usulután', 7);
INSERT INTO `alumno` VALUES (10, 'Martin Benitez Benitez', 1, '054789357-2', 'Masculino', 'mb2812@gmail.com', '2789-6389', 'San Miguel', 7);

-- ----------------------------
-- Table structure for carrera
-- ----------------------------
DROP TABLE IF EXISTS `carrera`;
CREATE TABLE `carrera`  (
  `codCarrera` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCarrera` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `titulo` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `codFacultad` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codCarrera`) USING BTREE,
  INDEX `FK_facultad`(`codFacultad`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of carrera
-- ----------------------------
INSERT INTO `carrera` VALUES (1, 'Ingeniería en Sistemas', 'Ingeniero en Sistemas', 1);
INSERT INTO `carrera` VALUES (2, 'Técnico en Sistemas', 'Técnico en Sistemas', 1);
INSERT INTO `carrera` VALUES (3, 'Administración de Redes', 'Adminsitrador de Redes de Computadoras', 1);
INSERT INTO `carrera` VALUES (4, 'Administración de Bases de Datos', 'Administrador de Bases de Datos', 1);
INSERT INTO `carrera` VALUES (5, 'Tecnico en Administración de Redes', 'Técnico Administrador de Redes', 1);
INSERT INTO `carrera` VALUES (6, 'Licenciatura en Administración de Empresas', 'Licenciado en Administración de Empresas', 2);
INSERT INTO `carrera` VALUES (7, 'Licenciatura en Contaduría Pública', 'Licenciado en Contaduría Pública', 2);
INSERT INTO `carrera` VALUES (8, 'Técnico en Contaduría Pública', 'Técnico en Contaduría Pública', 2);
INSERT INTO `carrera` VALUES (9, 'Licenciatura en Mercadeo', 'Licenciado en Mercadeo', 2);
INSERT INTO `carrera` VALUES (10, 'Técnico en Mercadeo', 'Técnico en Mercadeo', 2);

-- ----------------------------
-- Table structure for facultad
-- ----------------------------
DROP TABLE IF EXISTS `facultad`;
CREATE TABLE `facultad`  (
  `codFacultad` int(11) NOT NULL AUTO_INCREMENT,
  `nombreFacultad` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  PRIMARY KEY (`codFacultad`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of facultad
-- ----------------------------
INSERT INTO `facultad` VALUES (1, 'Facultad de Informática');
INSERT INTO `facultad` VALUES (2, 'Facultad de Ciencias Empresariales');

-- ----------------------------
-- Table structure for inscripcion
-- ----------------------------
DROP TABLE IF EXISTS `inscripcion`;
CREATE TABLE `inscripcion`  (
  `codInscripcion` int(11) NOT NULL,
  `fecha` datetime(0) NULL DEFAULT NULL,
  `estado` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `codAlumno` int(11) NOT NULL AUTO_INCREMENT,
  `codMateria` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codInscripcion`) USING BTREE,
  INDEX `FK_alumno`(`codAlumno`) USING BTREE,
  INDEX `FK_materia`(`codMateria`) USING BTREE,
  CONSTRAINT `FK_materia` FOREIGN KEY (`codMateria`) REFERENCES `materia` (`codMateria`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of inscripcion
-- ----------------------------
INSERT INTO `inscripcion` VALUES (1, '2020-04-16 12:11:44', 'Activo', 1, 1);
INSERT INTO `inscripcion` VALUES (2, '2020-04-16 12:11:44', 'Activo', 2, 1);
INSERT INTO `inscripcion` VALUES (3, '2020-04-16 12:11:44', 'Activo', 2, 2);
INSERT INTO `inscripcion` VALUES (4, '2020-04-16 12:11:44', 'Activo', 3, 4);
INSERT INTO `inscripcion` VALUES (5, '2020-04-16 12:11:44', 'Activo', 4, 3);
INSERT INTO `inscripcion` VALUES (6, '2020-04-16 12:11:44', 'Activo', 5, 6);
INSERT INTO `inscripcion` VALUES (7, '2020-04-16 12:11:44', 'Activo', 6, 5);
INSERT INTO `inscripcion` VALUES (8, '2020-04-16 12:11:44', 'Activo', 6, 6);
INSERT INTO `inscripcion` VALUES (9, '2020-04-16 12:12:56', 'Activo', 8, 9);
INSERT INTO `inscripcion` VALUES (10, '2020-04-16 12:12:56', 'Activo', 7, 7);
INSERT INTO `inscripcion` VALUES (11, '2020-04-16 12:15:49', 'Activo', 7, 8);
INSERT INTO `inscripcion` VALUES (12, '2020-04-16 12:15:50', 'Activo', 8, 10);
INSERT INTO `inscripcion` VALUES (13, '2020-04-16 12:15:50', 'Activo', 9, 13);
INSERT INTO `inscripcion` VALUES (14, '2020-04-16 12:15:50', 'Activo', 9, 14);
INSERT INTO `inscripcion` VALUES (15, '2020-04-16 12:15:50', 'Activo', 10, 14);

-- ----------------------------
-- Table structure for materia
-- ----------------------------
DROP TABLE IF EXISTS `materia`;
CREATE TABLE `materia`  (
  `codMateria` int(11) NOT NULL,
  `nombreMateria` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NULL DEFAULT NULL,
  `codCarrera` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codMateria`) USING BTREE,
  INDEX `FK_carreraM`(`codCarrera`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_spanish2_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of materia
-- ----------------------------
INSERT INTO `materia` VALUES (1, 'Programación de Computadoras', 1);
INSERT INTO `materia` VALUES (2, 'Administración de Sistemas', 1);
INSERT INTO `materia` VALUES (3, 'Programación de Sistemas de Computo', 2);
INSERT INTO `materia` VALUES (4, 'Introducción a los Sistemas Informáticos', 2);
INSERT INTO `materia` VALUES (5, 'Telecomunicaciones', 3);
INSERT INTO `materia` VALUES (6, 'Introducción a las Redes de Computadoras', 3);
INSERT INTO `materia` VALUES (7, 'Sistemas Gestores de Bases de Datos', 4);
INSERT INTO `materia` VALUES (8, 'Programación de Bases de Datos', 4);
INSERT INTO `materia` VALUES (9, 'Redes I', 5);
INSERT INTO `materia` VALUES (10, 'Redes II', 5);
INSERT INTO `materia` VALUES (11, 'Mátematica Básica', 6);
INSERT INTO `materia` VALUES (12, 'Estadistica I', 6);
INSERT INTO `materia` VALUES (13, 'Presupuestos', 7);
INSERT INTO `materia` VALUES (14, 'Derecho Mercantil y Laboral', 7);
INSERT INTO `materia` VALUES (15, 'Contabilidad I', 8);
INSERT INTO `materia` VALUES (16, 'Contabilidad de Costos', 8);
INSERT INTO `materia` VALUES (17, 'Sociología General', 9);
INSERT INTO `materia` VALUES (18, 'Administración de Ventas', 9);
INSERT INTO `materia` VALUES (19, 'Economía I', 10);
INSERT INTO `materia` VALUES (20, 'Macroeconomía', 10);

SET FOREIGN_KEY_CHECKS = 1;
