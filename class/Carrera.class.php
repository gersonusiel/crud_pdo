<?php
include "Crud.class.php";
$crud = new Crud();

class Carrera
{
    public $codigo;
    public $nombre;
    public $titulo;
    public $codFacultad;

    public function getAll()
    {
        global $crud;
        $crud->sql = "SELECT * FROM carrera";
        return $crud->selectRows();
    }

    public function save()
    {
        global $crud;
        $crud->sql = "INSERT INTO carrera
            (
                nombreCarrera,
                titulo,
                codFacultad
            )
            VALUES
            (
                :nombre,
                :titulo,
                :codFacultad
            )";
        $vals = array(
            ":nombre" => $this->nombre,
            ":titulo" => $this->titulo,
            ":codFacultad" => $this->codFacultad
        );
        $crud->insert($vals);
    }

    public function update()
    {
        global $crud;
        $crud->sql = "UPDATE carrera SET
            nombreCarrera=:nombre,
            titulo=:titulo,
            codFacultad=:codFacultad
            WHERE codCarrera=:codigo";

        $vals = array(
            ":codigo" => $this->codigo,
            ":nombre" => $this->nombre,
            ":titulo" => $this->titulo,
            ":codFacultad" => $this->codFacultad
        );
        $crud->updateByID($vals);
    }
    public function getByCod()
    {
        global $crud;
        $crud->sql = "SELECT * FROM carrera WHERE codCarrera=:codCarrera";
        $id = array(
            ":codCarrera" => $this->codigo
        );
        return $crud->getByID($id);
    }
    public function delete()
    {
        global $crud;
        $crud->sql = "DELETE FROM carrera WHERE codCarrera=:codigo";
        $vals = array(
            ":codigo" => $this->codigo
        );
        $crud->deleteById($vals);
    }

}

    // $carrera = new Carrera();

    //INSERTAR DATO
    // $carrera->nombre = "Licenciatura en idiomas";
    // $carrera->titulo = "Licenciado en idiomas";
    // $carrera->codFacultad = 2;
    // $carrera->save();


    //ACTUALIZAR DATO
    // $carrera->codigo = 11;
    // $carrera->nombre = "Licenciatura en ingles";
    // $carrera->titulo = "Licenciado en ingles";
    // $carrera->codFacultad = 2;
    // $carrera->update();

    //ELIMINAR REGISTRO
    // $carrera->codigo  = 11;
    // $carrera->delete();

    // echo "<pre>";
    // print_r($carrera->getAll());
    // echo "</pre>";
