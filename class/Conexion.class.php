<?php
    class Conexion{
        public $dbh;

        public function __construct()
        {
            try {
                $dsn  = "mysql:host=127.0.0.1;dbname=registroAcademico";
                $this->dbh= New PDO($dsn, "root", "");
                $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

?>