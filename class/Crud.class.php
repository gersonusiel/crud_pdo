<?php
    include "Conexion.class.php";
    $pdo = new Conexion();

    class Crud{
        public $sql;

        public function selectRows(){
            try {
                global $pdo;
                $stmt = $pdo->dbh->prepare($this->sql);
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $stmt->execute();
                $rows = $stmt->fetchAll();
                return $rows;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }

        public function insert($values){
            try {
                global $pdo;
                $stmt = $pdo->dbh->prepare($this->sql);
                $stmt->execute($values);
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }

        public function updateByID($values){
            try {
                global $pdo;
                $stmt = $pdo->dbh->prepare($this->sql);
                $stmt->execute($values);
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }

        public function deleteById($id){
            try {
                global $pdo;
                $stmt = $pdo->dbh->prepare($this->sql);
                $stmt->execute($id);
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
        public function getByID($id)
        {
            try{
                global $pdo;
                $stmt= $pdo->dbh->prepare($this->sql);        
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $stmt->execute($id);
                $rows = $stmt->fetchAll();
                return $rows;
            }catch(PDOException $e){
                echo $e->getMessage();
            }
    }
}

?>