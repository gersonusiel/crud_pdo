<?php
    // include "Crud.class.php";
    // $crud = new Crud();

    class Alumno{
        public $codigo; 
        public $nombre; 
        public $tipoDocumento; 
        public $numeroDocumento; 
        public $sexo; 
        public $correo; 
        public $telefono; 
        public $direccion; 
        public $codCarrera;

        public function getAll(){
            global $crud;
            $crud->sql="SELECT * FROM Alumno";
            return $crud->selectRows();
        }

        public function save(){
            global $crud;
            $crud->sql="INSERT INTO alumno
            (
                nombreCompleto,
                tipoDocumento,
                numeroDocumento,
                sexo,
                correo,
                numeroTelefono,
                direccion,
                codCarrera
            )
                VALUES
            (
                :nombre,
                :tipoDocumento,
                :numeroDocumento,
                :sexo,
                :correo,
                :telefono,
                :direccion,
                :codCarrera
            )";
            $vals = array(
                ":nombre"=>$this->nombre,
                ":tipoDocumento"=>$this->tipoDocumento,
                ":numeroDocumento"=>$this->numeroDocumento,
                ":sexo"=>$this->sexo,
                ":correo"=>$this->correo,
                ":telefono"=>$this->telefono,
                ":direccion"=>$this->direccion,
                ":codCarrera"=>$this->codCarrera
            );
            $crud->insert($vals);
        }

        public function update(){
            global $crud;
            $crud->sql="UPDATE alumno SET
            nombreCompleto=:nombre,
            tipoDocumento=:tipoDocumento,
            numeroDocumento=:numeroDocumento,
            sexo=:sexo,
            correo=:correo,
            numeroTelefono=:telefono,
            direccion=:direccion,
            codCarrera=:codCarrera
            WHERE codAlumno=:codigo";

            $vals = array(
                ":codigo"=>$this->codigo,
                ":nombre"=>$this->nombre,
                ":tipoDocumento"=>$this->tipoDocumento,
                ":numeroDocumento"=>$this->numeroDocumento,
                ":sexo"=>$this->sexo,
                ":correo"=>$this->correo,
                ":telefono"=>$this->telefono,
                ":direccion"=>$this->direccion,
                ":codCarrera"=>$this->codCarrera
            );
            $crud->updateByID($vals);
        }
        public function getByCod()
    {
        global $crud;
        $crud->sql="SELECT * FROM alumno WHERE codAlumno=:codigo";
        $id= array(
            ":codigo"=>$this->codigo
        );
        return $crud->getByID($id);
    }

        public function delete(){
            global $crud;
            $crud->sql="DELETE FROM alumno WHERE codAlumno=:codigo";
            $vals = array(":codigo"=>$this->codigo);
            $crud->deleteById($vals);
        }
    }

    // $alumno = new Alumno();

    //INSERTAMOS UN NUEVO ALUMNO
    // $alumno->codigo = 55;
    // $alumno->nombre = "Jonatan Armando Flores";
    // $alumno->tipoDocumento = 1;
    // $alumno->numeroDocumento = "0248383-8";
    // $alumno->sexo = "Masculino";
    // $alumno->correo = "jflores9292@gmail.com";
    // $alumno->telefono = "7537-2837";
    // $alumno->direccion = "San Miguel";
    // $alumno->save();

    // EDITAMOS LOS DATOS
    // $alumno->codigo = 55;
    // $alumno->nombre = "Jonatan Armando Flores García";
    // $alumno->tipoDocumento = 1;
    // $alumno->numeroDocumento = "0248383-8";
    // $alumno->sexo = "Masculino";
    // $alumno->correo = "jflores9292@gmail.com";
    // $alumno->telefono = "7537-2837";
    // $alumno->direccion = "Usulután";
    // $alumno->update();

    //ELIMINADO UN ALUMNO
    // $alumno->codigo = 55;
    // $alumno->delete();

    //MOSTRANOS LOS DATOS
    // echo "<pre>";
    // print_r($alumno->getAll());
    // echo "</pre>";
?>