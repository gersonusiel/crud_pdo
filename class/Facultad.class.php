<?php
// include "Crud.class.php";
// $crud = new Crud();

class Facultad
{
    public $codigo;
    public $nombre;

    public function getAll()
    {
        global $crud;
        $crud->sql = "SELECT * FROM facultad";
        return $crud->selectRows();
    }

    public function save()
    {
        global $crud;
        $crud->sql = "INSERT INTO facultad
            (
                nombreFacultad
            )
            VALUES
            (
                :nombre,
            )";
        $vals = array(
            ":nombre" => $this->nombre
        );
        $crud->insert($vals);
    }

    public function update()
    {
        global $crud;
        $crud->sql = "UPDATE Facultad SET
            nombreFacultad=:nombre
            WHERE codFacultad=:codigo";

        $vals = array(
            ":codigo" => $this->codigo,
            ":nombre" => $this->nombre
        );
        $crud->updateByID($vals);
    }
    public function getByCodFacultad()
    {
        global $crud;
        $crud->sql = "SELECT * FROM facultad WHERE codFacultad=:codFacultad";
        $id = array(
            ":codFacultad" => $this->codigo
        );
        return $crud->getByID($id);
    }
    public function delete()
    {
        global $crud;
        $crud->sql = "DELETE FROM Facultad WHERE codFacultad=:codigo";
        $vals = array(
            ":codigo" => $this->codigo
        );
        $crud->deleteById($vals);
    }
}

    // $carrera = new Carrera();

    //INSERTAR DATO
    // $carrera->nombre = "Licenciatura en idiomas";
    // $carrera->titulo = "Licenciado en idiomas";
    // $carrera->codFacultad = 2;
    // $carrera->save();


    //ACTUALIZAR DATO
    // $carrera->codigo = 11;
    // $carrera->nombre = "Licenciatura en ingles";
    // $carrera->titulo = "Licenciado en ingles";
    // $carrera->codFacultad = 2;
    // $carrera->update();

    //ELIMINAR REGISTRO
    // $carrera->codigo  = 11;
    // $carrera->delete();

    // echo "<pre>";
    // print_r($carrera->getAll());
    // echo "</pre>";
